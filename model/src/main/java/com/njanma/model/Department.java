package com.njanma.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author Hryshanau Andrei.
 *
 */
@Entity
@Table(name="departments")
public class Department {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@Column(name="name")
	private String name;
	
	@Column(name="salary")
	private Double salary;
	
	@JsonIgnore
	@OneToMany(
			fetch=FetchType.LAZY, 
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="departmentId") 
	private List<Employee> allEmployees; 
	
	public Department() {
		super();
	}
	
	public Department(String name) {
		super();
		this.name = name;
	}

	public Department(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	public List<Employee> getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(List<Employee> allEmployees) {
		this.allEmployees = allEmployees;
	}

	@JsonIgnore
	public boolean isNew(){
		if(this.id!=null){
			return false;
		}
		return true;
	}
	
	@JsonIgnore
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", salary=" + salary + ", allEmployees=" + allEmployees
				+ "]";
	}
	
}
