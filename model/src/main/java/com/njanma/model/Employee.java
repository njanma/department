package com.njanma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.hibernate.annotations.Type;

/**
 * 
 * @author Hryshanau Andrei.
 *
 */
@Entity
@Table(name = "employees")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@Column(name = "full_name")
	private String fullName;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(name = "DOB")
	@JsonSerialize(using=DateSerializer.class)
	@JsonDeserialize(using=DateDeserializer.class)
	private LocalDate dateOfBirthday;

	@Column(name = "salary")
	private Double salary;
	
	@Column(name="department_id")
	private Long departmentId;
	
	public Employee() {
		super();
	}
	
	public Employee(Long id, String fullName, LocalDate dateOfBirthday, Double salary) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.dateOfBirthday = dateOfBirthday;
		this.salary = salary;
	}

	public Employee(Long id, String fullName, LocalDate dateOfBirthday, Double salary, Long departmentId) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.dateOfBirthday = dateOfBirthday;
		this.salary = salary;
		this.departmentId = departmentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public LocalDate getDateOfBirthday() {
		return dateOfBirthday;
	}

	public void setDateOfBirthday(LocalDate dateOfBirthday) {
		this.dateOfBirthday = dateOfBirthday;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	
	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	@JsonIgnore
	@Override
	public String toString() {
		return "Employee [id=" + id + ", fullName=" + fullName + ", dateOfBirthday=" + dateOfBirthday + ", salary="
				+ salary + ", departmentId=" + departmentId + "]";
	}

	@JsonIgnore
	public boolean isNew() {
		if (this.id != null) {
			return false;
		}
		return true;
	}
}
