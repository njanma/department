package com.njanma.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.njanma.model.Department;
import com.njanma.model.Employee;
import com.njanma.service.DepartmentService;
import com.njanma.service.EmployeeService;

/**
 * 
 * @author Hryshanau Andrei.
 *
 *	REST resource for {@link Department}
 */
@RestController
@RequestMapping(value = "/departments")
public class DepartmentResource {

	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private EmployeeService employeeService;

	Logger log = Logger.getLogger(DepartmentResource.class);

	/**
	 * Get all departments.
	 * 
	 * @return all departments.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Iterable<Department>> getAllDepartments() {
		if (log.isDebugEnabled()) {
			log.debug("getAllDepartments()");
		}
		Iterable<Department> allDepartments = departmentService.getAllDepartments();
		if (allDepartments.iterator().hasNext() == false) {
			return new ResponseEntity<Iterable<Department>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Iterable<Department>>(allDepartments, HttpStatus.OK);
	}

	/**
	 * Get department by ID.
	 * 
	 * @param id department ID.
	 * @return department.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Department> getByIdDepartment(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("getByIdDepartment(" + id + ")");
		}
		Department currentDepartment = departmentService.getDepartmentById(Long.valueOf(id));
		if (currentDepartment == null) {
			return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
		} else
			return new ResponseEntity<Department>(currentDepartment, HttpStatus.OK);
	}
	
	/**
	 * Get all employees in department by ID
	 * 
	 * @param id department ID
	 * @return is OK
	 */
	@RequestMapping(value="/{id}/employees", method=RequestMethod.GET)
	public ResponseEntity<Iterable<Employee>> getAllEmployeesInDepartment(
			@PathVariable("id") String id){
		if (log.isDebugEnabled()) {
			log.debug("getAllEmployeesInDepartment(" +"id = "+ id + ")");
		}
		Iterable<Employee> allEmployeesInDepartment = employeeService.getAllEmployeesInDepartmentById(Long.valueOf(id));
		return new ResponseEntity<Iterable<Employee>>(allEmployeesInDepartment,HttpStatus.OK);
	}
	

	/**
	 * Delete department by ID.
	 * 
	 * @param id department ID.
	 * @return status 204.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Department> deleteDepartmentById(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById(" + id + ")");
		}
		Department currentDepartment = departmentService.getDepartmentById(Long.valueOf(id));
		if (currentDepartment == null) {
			return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
		}
		departmentService.deleteDepartmentById(Long.valueOf(id));
		return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Update department by ID.
	 * 
	 * @param id department id
	 * @param parameters parameters request.
	 * @return status 200.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Department> updateDepartmentById(@PathVariable("id") String id,
			@RequestBody Department parameters) {
		if (log.isDebugEnabled()) {
			log.debug("updateDepartmentById(" + "id=" + id + ", parameters=" + parameters.toString() + ")");
		}
		if (departmentService.getDepartmentById(Long.valueOf(id)) == null) {
			return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
		}
		departmentService.updateDepartment(Long.valueOf(id), parameters);
		return new ResponseEntity<Department>(HttpStatus.OK);
	}

	/**
	 * Create department.
	 * 
	 * @param department created department.
	 * @return status 201.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Department> createDepartment(@RequestBody Department department) {
		if (log.isDebugEnabled()) {
			log.debug("createDepartment(" + department.toString() + ")");
		}
		if (department.getId() != null && departmentService.isDepartmentExist(department)) {
			return new ResponseEntity<Department>(HttpStatus.CONFLICT);
		}
		departmentService.saveDepartment(department);
		return new ResponseEntity<Department>(HttpStatus.CREATED);
	}
	
}
