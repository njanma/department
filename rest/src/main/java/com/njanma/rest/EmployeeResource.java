package com.njanma.rest;

import java.text.ParseException;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.njanma.model.Employee;
import com.njanma.service.EmployeeService;

/**
 * 
 * @author Hryshanau Andrei.
 * 
 *         REST resource for {@link Employee}
 *
 */
@RestController
@RequestMapping("/employees")
public class EmployeeResource {

	@Autowired
	private EmployeeService employeeService;

	Logger log = Logger.getLogger(EmployeeResource.class);

	/**
	 * Get all emploees. If there are parameters, the search between dates.
	 * 
	 * @param from begin of interval.
	 * @param end of interval.
	 * @return all employees or employees beetween request date.
	 * @throws ParseException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Iterable<Employee>> getAllEmploees(
			@RequestParam(value = "from", required = false) String from,
			@RequestParam(value = "to", required = false) String to) throws ParseException {
		if (log.isDebugEnabled()) {
			log.debug("getAllEmploees(" + "parameters=" + "afterDate" + from + ", beforeDate=" + to + ")");
		}
		DateTimeFormatter formatter = DateTimeFormat.forPattern("y-M-d");
		Iterable<Employee> allEmployees = employeeService.getAllEmploees();
		if (allEmployees.iterator().hasNext() == false) {
			return new ResponseEntity<Iterable<Employee>>(HttpStatus.NO_CONTENT);
		}
		if (from != null && !from.equals("") && to.equals("")) {
			LocalDate dateOfBirthday = formatter.parseLocalDate(from);
			return new ResponseEntity<Iterable<Employee>>(Arrays.asList(employeeService.findEmployeeByDateOfBirthday(dateOfBirthday)),
					HttpStatus.OK);
		}
		if (from != null && !from.equals("") && to != null && !to.equals("")) {
			LocalDate before = formatter.parseLocalDate(from);
			LocalDate after = formatter.parseLocalDate(to);
			return new ResponseEntity<Iterable<Employee>>(employeeService.findEmployeeByDateBetween(before, after),
					HttpStatus.OK);
		}

		return new ResponseEntity<Iterable<Employee>>(employeeService.getAllEmploees(), HttpStatus.OK);
	}

	/**
	 * Get employee by ID.
	 * 
	 * @param id department ID.
	 * @return employee.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Employee> getByIdEmployee(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("getByIdDepartment(" + id + ")");
		}
		Employee currentEmployee = employeeService.getEmployeeById(Long.valueOf(id));
		if (currentEmployee == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		} else
			return new ResponseEntity<Employee>(currentEmployee, HttpStatus.OK);
	}

	/**
	 * Delete employee by ID.
	 * 
	 * @param id deparment ID.
	 * @return status 204.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Employee> deleteEmployeeById(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById(" + id + ")");
		}
		Employee currentEmployee = employeeService.getEmployeeById(Long.valueOf(id));
		if (currentEmployee == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		employeeService.deleteEmployeeById(Long.valueOf(id));
		return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Update employee by ID.
	 * 
	 * @param id department ID
	 * @param parameters parameters request.
	 * @return status 200.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Employee> updateEmployeeById(@PathVariable("id") String id,
			@RequestBody Employee parameters) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById(" + "id=" + id + ", parameters=" + parameters.toString() + ")");
		}
		if (employeeService.getEmployeeById(Long.valueOf(id)) == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		employeeService.updateEmployee(Long.valueOf(id), parameters);
		return new ResponseEntity<Employee>(HttpStatus.OK);
	}

	/**
	 * Create department.
	 * 
	 * @param employee parameters create emplooyee.
	 * @return status 201.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Employee> createDepartment(@RequestBody Employee employee) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById(" + "parameters=" + employee.toString() + ")");
		}
		if (employee.getId() != null && employeeService.isEmployeeExist(employee)) {
			return new ResponseEntity<Employee>(HttpStatus.CONFLICT);
		}
		employeeService.saveEmployee(employee);
		return new ResponseEntity<Employee>(HttpStatus.CREATED);
	}

}