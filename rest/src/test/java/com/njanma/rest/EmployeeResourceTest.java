package com.njanma.rest;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.dao.EmployeeRepository;
import com.njanma.model.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/restContextForTest.xml" })
@Transactional
public class EmployeeResourceTest {

	Employee employeeOne;
	Employee employeeTwo;

	@Value("${rest.employees}")
	String EMPLOYEES_URL;

	MockMvc mockMvc;

	ObjectMapper mapper;

	@Autowired
	EmployeeResource employeeResource;

	@Autowired
	EmployeeRepository employeeRepository;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(employeeResource)
				.setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
		mapper = new ObjectMapper();
		employeeOne = employeeRepository.findOne(1l);
		employeeTwo = employeeRepository.findOne(2l);
	}

	@Test
	public final void testGetAllEmploees() throws Exception {
		String response = mapper.writeValueAsString(Arrays.asList(employeeOne, employeeTwo));
		mockMvc.perform(get(EMPLOYEES_URL))
				// .andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(response));
	}

	@Test
	public final void testGetEmployeesBetweenDate()throws Exception{
		String response = mapper.writeValueAsString(Arrays.asList(employeeTwo));
		mockMvc.perform(get(EMPLOYEES_URL)
				.param("from", "1994-01-01")
				.param("to", "1996-01-01"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string(response));
	}
	
	@Test
	public final void testGetEmployeeByBirthdate()throws Exception{
		String response = mapper.writeValueAsString(Arrays.asList(employeeOne));
		mockMvc.perform(get(EMPLOYEES_URL)
				.param("from", "1991-01-01")
				.param("to", ""))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string(response));
	}

	@Test
	public final void testGetByIdDepartment() throws Exception {
		String response = mapper.writeValueAsString(employeeOne);
		mockMvc.perform(get(EMPLOYEES_URL + "/" +1)).andExpect(status().isOk()).andExpect(content().string(response));
	}

	@Test
	public final void testDeleteDepartmentById() throws Exception {
		mockMvc.perform(delete(EMPLOYEES_URL +"/" +1)).andExpect(status().isNoContent());
		List<Employee> employees = (List<Employee>) employeeRepository.findAll();
		assertEquals(1, employees.size());
		assertEquals(Long.valueOf(2), employees.get(0).getId());
	}

	@Test
	public final void testUpdateDepartmentById() throws Exception {
		employeeOne.setFullName("worker three");
		String body = mapper.writeValueAsString(employeeOne);
		mockMvc.perform(put(EMPLOYEES_URL + "/" +1).contentType(MediaType.APPLICATION_JSON).content(body))
				.andExpect(status().isOk());
		Employee employee = employeeRepository.findOne(1l);
		assertEquals("worker three", employee.getFullName());
	}

	@Test
	public final void testCreateDepartment() throws Exception {
		String body = mapper.writeValueAsString(new Employee(3l, "worker three", new LocalDate(1991, 5, 15), 350.0));
		mockMvc.perform(post(EMPLOYEES_URL).contentType(MediaType.APPLICATION_JSON).content(body))
				.andExpect(status().isCreated());
		List<Employee> employees = (List<Employee>) employeeRepository.findAll();
		assertEquals(3, employees.size());
		assertEquals("worker three", employees.get(2).getFullName());
	}

}
