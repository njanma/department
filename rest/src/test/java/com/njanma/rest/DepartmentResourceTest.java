package com.njanma.rest;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.dao.DepartmentRepository;
import com.njanma.model.Department;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/restContextForTest.xml" })
@Transactional
public class DepartmentResourceTest {

	Department departmentOne;
	Department departmentTwo;
	
	@Value("${rest.departments}")
	String DEPARTMENTS_URL;

	MockMvc mockMvc;

	ObjectMapper mapper;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	DepartmentResource departmentResource;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(departmentResource)
				.setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
		mapper = new ObjectMapper();

		departmentOne = departmentRepository.findOne(1l);
		departmentTwo = departmentRepository.findOne(2l);
	}

	@Test
	public final void testGetAllDepartments() throws Exception {
		departmentOne.setSalary(1000.0);
		departmentTwo.setSalary(950.0);
		String response = mapper.writeValueAsString(Arrays.asList(departmentOne, departmentTwo));
		mockMvc.perform(get(DEPARTMENTS_URL))
				// .andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(response));
	}

	@Test
	public final void testGetByIdDepartment() throws Exception {
		departmentOne.setSalary(1000.0);
		String response = mapper.writeValueAsString(departmentOne);
		mockMvc.perform(get(DEPARTMENTS_URL+"/" + 1))
				// .andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(response));
	}

	@Test
	public final void testDeleteDepartmentById() throws Exception {
		mockMvc.perform(delete(DEPARTMENTS_URL+"/" + 1))
				// .andDo(print())
				.andExpect(status().isNoContent());
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		assertEquals(1, departments.size());
		assertEquals(Long.valueOf(2), departments.get(0).getId());
	}

	@Test
	public final void testUpdateDepartmentById() throws Exception {
		String actualName = "departmentThree";
		departmentOne.setName(actualName);
		String body = mapper.writeValueAsString(departmentOne);
		mockMvc.perform(put(DEPARTMENTS_URL+"/" + 1).contentType(MediaType.APPLICATION_JSON).content(body))
				// .andDo(print())
				.andExpect(status().isOk());

		Department department = departmentRepository.findOne(1l);
		assertEquals(actualName, department.getName());
	}

	@Test
	public final void testCreateDepartment() throws Exception {
		String newDepartment = mapper.writeValueAsString(new Department(3l, "departmentThree"));
		mockMvc.perform(post(DEPARTMENTS_URL).contentType(MediaType.APPLICATION_JSON).content(newDepartment))
				.andExpect(status().isCreated());
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		assertEquals(3, departments.size());
		assertEquals("departmentThree", departments.get(2).getName());
	}

}
