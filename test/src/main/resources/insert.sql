INSERT INTO `departments` (`id`, `name`) VALUES ('1', 'departOne');
INSERT INTO `departments` (`id`, `name`) VALUES ('2', 'departTwo');

INSERT INTO `employees` (`id`, `full_name`, `DOB`, `salary`, `department_id`) VALUES ('1', 'employeeOne', '1991-01-01', '1000', '1');
INSERT INTO `employees` (`id`, `full_name`, `DOB`, `salary`, `department_id`) VALUES ('2', 'employeeTwo', '1995-01-01', '950', '2');


