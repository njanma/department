package com.njanma.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njanma.dao.DepartmentRepository;
import com.njanma.model.Department;
import com.njanma.service.DepartmentService;

@Service
public class DefaultDepartmentService implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	private Logger log = Logger.getLogger(DefaultEmployeeService.class);

	public List<Department> getAllDepartments() {
		if (log.isDebugEnabled()) {
			log.debug("getAllDepartments()");
		}
		List<Department> departments = departmentRepository.findAll();
		return departments;
	}

	public Department getDepartmentById(Long id) {
		if (log.isDebugEnabled()) {
			log.debug("getDepartmentById("+id+")");
		}
		Department currentDepartment = departmentRepository.findById(id);
		return currentDepartment;
	}

	public void deleteDepartmentById(Long id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById("+id+")");
		}
		departmentRepository.delete(id);
		log.info("Department: id="+id+", successfully deleted!");
	}

	public void saveDepartment(Department department) {
		if (log.isDebugEnabled()) {
			log.debug("saveDepartment("+department.toString()+")");
		}
		if (department == null) {
			log.error("Department must be not null");
			throw new IllegalArgumentException("Department must be not null");
		}
		Department saveDep = departmentRepository.save(department);
		log.info(saveDep.toString()+", successfully inserted!");
	}

	public void updateDepartment(Long id, Department department) {
		if (log.isDebugEnabled()) {
			log.debug("updateDepartment("+"id="+id+", parameters="+department.toString()+")");
		}
		Department currentDepartment = departmentRepository.findOne(id);
		if (department.getName() != null) {
			currentDepartment.setName(department.getName());
		}
		Department updateDep = departmentRepository.save(currentDepartment);
		log.info(updateDep.toString()+", successfully updated!");
	}

	public boolean isDepartmentExist(Department department) {
		if (log.isDebugEnabled()) {
			log.debug("isDepartmentExist("+department.toString()+")");
		}
		return departmentRepository.exists(department.getId());
	}

}
