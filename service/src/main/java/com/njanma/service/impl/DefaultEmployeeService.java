package com.njanma.service.impl;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njanma.dao.EmployeeRepository;
import com.njanma.model.Employee;
import com.njanma.service.EmployeeService;

@Service
public class DefaultEmployeeService implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	Logger log = Logger.getLogger(DefaultEmployeeService.class);

	public Iterable<Employee> getAllEmploees() {
		if (log.isDebugEnabled()) {
			log.debug("getAllEmploees()");
		}
		return employeeRepository.findAll();
	}
	
	public Iterable<Employee> getAllEmployeesInDepartmentById(Long id) {
		if (log.isDebugEnabled()) {
			log.debug("getAllEmployeesInDepartmentById( id="+id+" )");
		}
		return employeeRepository.findByDepartmentId(id);
	}

	public Employee getEmployeeById(Long id) {
		if (log.isDebugEnabled()) {
			log.debug("getEmployeeById(" + id + ")");
		}
		return employeeRepository.findOne(id);
	}

	public void deleteEmployeeById(Long id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteEmployeeById(" + id + ")");
		}
		employeeRepository.delete(id);
		log.info("Employee: id=" + id + ", successfully deleted!");
	}

	/**
	 * @throws IllegalArgumentException
	 *             if employee reference is null.
	 */
	public void saveEmployee(Employee employee) {
		if (log.isDebugEnabled()) {
			log.debug("saveEmployee(" + employee.toString() + ")");
		}
		if (employee == null) {
			log.error("Employee must be not null");
			throw new IllegalArgumentException("Employee must be not null");
		}
		Employee saveEmployee = employeeRepository.save(employee);
		log.info(saveEmployee.toString() + ", successfully inserted!");
	}

	public void updateEmployee(Long id, Employee employee) {
		if (log.isDebugEnabled()) {
			log.debug("updateEmployee(" + "id=" + id + ", parameters=" + employee.toString() + ")");
		}
		Employee currentEmployee = employeeRepository.findOne(id);
		if (employee.getFullName() != null) {
			currentEmployee.setFullName(employee.getFullName());
		}
		if (employee.getDateOfBirthday() != null) {
			currentEmployee.setDateOfBirthday(employee.getDateOfBirthday());
		}
		if (employee.getSalary() != null) {
			currentEmployee.setSalary(employee.getSalary());
		}
		if (employee.getDepartmentId() != null) {
			currentEmployee.setDepartmentId(employee.getDepartmentId());
		}
		Employee updateEmployee = employeeRepository.save(currentEmployee);
		log.info(updateEmployee.toString() + ", successfully updated!");
	}

	public boolean isEmployeeExist(Employee employee) {
		if (log.isDebugEnabled()) {
			log.debug("isEmployeeExist(" + employee.toString() + ")");
		}
		return employeeRepository.exists(employee.getId());
	}

	public Iterable<Employee> findEmployeeByDateBetween(LocalDate from, LocalDate to) {
		if (log.isDebugEnabled()) {
			log.debug("findEmployeeByDateBetween(" + "from=" + from + ", to=" + to + ")");
		}
		return employeeRepository.findByDateOfBirthdayBetween(from, to);
	}

	public Employee findEmployeeByDateOfBirthday(LocalDate dateOfBirthday) {
		if (log.isDebugEnabled()) {
			log.debug("findEmployeeByDateOfBirthday( " + "dateOfBirthday=" + dateOfBirthday + " )");
		}
		return employeeRepository.findByDateOfBirthday(dateOfBirthday);
	}
	
	

}
