package com.njanma.service;

import org.joda.time.LocalDate;

import com.njanma.model.Employee;

/**
 * 
 * @author Hryshanau Andrei.
 *
 * Service for {@link Employee}
 */
public interface EmployeeService {
	
	/**
	 * Get all employees.
	 * 
	 * @return all employees.
	 */
	public Iterable<Employee> getAllEmploees();
	
	/**
	 * Get all employees in department.
	 * 
	 * @param id department ID.
	 * @return all employees.
	 */
	public Iterable<Employee> getAllEmployeesInDepartmentById(Long id);

	/**
	 * Get employee by ID.
	 * 
	 * @param id employee ID.
	 * @return employee.
	 */
	public Employee getEmployeeById(Long id);

	/**
	 * Delete employee by ID.
	 * 
	 * @param id employee ID.
	 */
	public void deleteEmployeeById(Long id);

	/**
	 * Save employee.
	 * 
	 * @param employee employee to save.
	 */
	public void saveEmployee(Employee employee);

	/**
	 * Update employee by ID.
	 * 
	 * @param id	   employee ID.
	 * @param employee employee parameters to update.
	 */
	public void updateEmployee(Long id, Employee employee);

	/**
	 * Check existence of a employee.
	 * 
	 * @param employee checked employee.
	 * @return existence.
	 */
	public boolean isEmployeeExist(Employee employee);
	
	/**
	 * Find employee by date of birthday.
	 * 
	 * @param dateOfBirthday date of birthday
	 * @return
	 */
	public Employee findEmployeeByDateOfBirthday(LocalDate dateOfBirthday);

	/**
	 * Find employees between two date.
	 * 
	 * @param from	begin of interval.
	 * @param to end of interval.
	 * @return all find employees between dates.
	 */
	public Iterable<Employee> findEmployeeByDateBetween(LocalDate from, LocalDate to);
}
