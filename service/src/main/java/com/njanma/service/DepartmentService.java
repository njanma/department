package com.njanma.service;

import java.util.List;

import com.njanma.model.Department;

/**
 * @author Hryshanau Andrei.
 *
 * Service for {@link Department}
 */
public interface DepartmentService {

	/**
	 * Get all departments.
	 * 
	 * return all departments.
	 */
	public List<Department> getAllDepartments();

	/**
	 * Get department by ID.
	 * 
	 * @param id department ID.
	 */
	public Department getDepartmentById(Long id);

	/**
	 * Delete department by ID.
	 * 
	 * @param id department ID.
	 */
	public void deleteDepartmentById(Long id);
	
	/**
	 * Save department.
	 * 
	 * @param department department to save.
	 */
	public void saveDepartment(Department department);

	/**
	 * Update department by ID.
	 * 
	 * @param id 		 department ID.
	 * @param department department parameters to update.
	 */
	public void updateDepartment(Long id, Department department);
	
	/**
	 * Check existence of a department.
	 * 
	 * @param department checked department.
	 * @return existence.
	 */
	public boolean isDepartmentExist(Department department);
}
