package com.njanma.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.njanma.dao.EmployeeRepository;
import com.njanma.model.Employee;

@RunWith(MockitoJUnitRunner.class)
public class DefaultEmployeeServiceTest {

	@Mock
	EmployeeRepository repository;

	@InjectMocks
	DefaultEmployeeService employeeService;

	@Mock
	Employee employeeOne;
	@Mock
	Employee employeeTwo;

	@Before
	public void setUp() throws Exception {
		when(employeeOne.getDepartmentId()).thenReturn(1l);
		when(employeeOne.getFullName()).thenReturn("employeeOne");
		when(repository.findOne(anyLong())).thenReturn(employeeOne);
		when(repository.save(any(Employee.class))).thenReturn(employeeTwo);
	}

	@Test
	public final void testGetAllEmploees() {
		employeeService.getAllEmploees();
		verify(repository, atLeastOnce()).findAll();
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public final void testGetAllEmployeesInDepartmentById(){
		employeeService.getAllEmployeesInDepartmentById(1l);
		verify(repository, atLeastOnce()).findByDepartmentId(1l);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testGetEmployeeById() {
		employeeService.getEmployeeById(1l);
		verify(repository, atLeastOnce()).findOne(anyLong());
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testDeleteEmployeeById() {
		employeeService.deleteEmployeeById(1l);
		verify(repository, atLeastOnce()).delete(anyLong());
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testSaveEmployee() {
		employeeService.saveEmployee(employeeOne);
		verify(repository).save(employeeOne);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testUpdateEmployee() {
		employeeService.updateEmployee(1l, employeeOne);
		verify(repository, atLeastOnce()).findOne(anyLong());
		verify(employeeOne, never()).getId();
		verify(employeeOne, atLeast(2)).getFullName();
		verify(employeeOne, atLeast(2)).getDepartmentId();
		verify(employeeOne, atLeastOnce()).getDateOfBirthday();
		verify(employeeOne, atLeastOnce()).getSalary();
		verify(repository).save(employeeOne);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testIsEmployeeExist() {
		employeeService.isEmployeeExist(employeeOne);
		verify(repository, atLeastOnce()).exists(anyLong());
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testFindByDateBetween() {
		LocalDate beforeDate = new LocalDate();
		LocalDate afterDate = new LocalDate();
		employeeService.findEmployeeByDateBetween(beforeDate, afterDate);
		verify(repository, atLeastOnce()).findByDateOfBirthdayBetween(any(LocalDate.class), any(LocalDate.class));
		verifyNoMoreInteractions(repository);
	}
	@Test
	public final void findEmployeeByDateOfBirthday() {
		LocalDate dateOfBirthday = new LocalDate();
		employeeService.findEmployeeByDateOfBirthday(dateOfBirthday);
		verify(repository).findByDateOfBirthday(any(LocalDate.class));
		verifyNoMoreInteractions(repository);
	}
	
	

}
