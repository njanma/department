package com.njanma.service.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.njanma.dao.DepartmentRepository;
import com.njanma.model.Department;

@RunWith(MockitoJUnitRunner.class)
public class DefaultDepartmentServiceTest {

	@Mock
	DepartmentRepository repository;

	@InjectMocks
	DefaultDepartmentService departmentService;

	@Mock
	Department departmentOne;
	@Mock
	Department departmentTwo;

	@Before
	public void setUp() throws Exception {
		when(departmentOne.getId()).thenReturn(1l);
		when(repository.findOne(anyLong())).thenReturn(departmentOne);
		when(repository.findAll()).thenReturn(Arrays.asList(departmentOne));
		when(repository.save(any(Department.class))).thenReturn(departmentTwo);
	}

	@Test
	public final void testGetAllDepartments() {
		departmentService.getAllDepartments();
		verify(repository, atLeastOnce()).findAll();
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testGetDepartmentById() {
		departmentService.getDepartmentById(1l);
		verify(repository,atLeastOnce()).findById(anyLong());
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testDeleteDepartmentById() {
		departmentService.deleteDepartmentById(1l);
		verify(repository,atLeastOnce()).delete(anyLong());
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testSaveDepartment() {
		departmentService.saveDepartment(departmentOne);
		verify(repository, atLeastOnce()).save(departmentOne);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testUpdateDepartment() {
		departmentService.updateDepartment(1l, departmentOne);
		verify(repository,atLeastOnce()).findOne(anyLong());
		verify(repository,atLeastOnce()).save(departmentOne);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public final void testIsDepartmentExist() {
		departmentService.isDepartmentExist(departmentOne);
		verify(repository).exists(anyLong());
		verifyNoMoreInteractions(repository);
	}

}
