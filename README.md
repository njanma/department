## Introduce

This demo application was written on Java and Spring 4 Framework.
In this application we have two *.war files, which can be deployed on your Apache Tomcat server.
First file is example of RESTful service. And second is Web application which use this service for displaying information.

Database stored on MySQL server.

In this example we have two entity: departments and employees. And we can get information about them from RESTful service in JSON format.

### How to use

1. Download source code: `git clone https://github.com/njanma/Department.git`.

2. You can find database initialization script in folder `/props/src/main/resources/init-script.sql`.
Add your configuration db pool and terms in `/props/src/main/resources/prod-db.properties` and `/props/src/main/resources/term-client.properties`. 

3. Then you must create war-files: `mvn install`.

4. Copy war-files which you can find in `/rest/target/rest.war` and `/web/target/web.war` to your tomcat server `webapps` folder.

5. And then you can see result in browser: `http://localhost:8080/web/`.

## REST api

| URL                               | Method | Description                                        |
|-----------------------------------|--------|----------------------------------------------------|
| "/rest/departments"               | GET    | Return all departments                             |
| "/rest/departments/{id}"          | GET    | Return department by ID 		                        |
| "/rest/departments"               | POST   | Create new department                              |
| "/rest/departments/{id}"          | PUT    | Update department by ID                            |
| "/rest/departments/{id}"          | DELETE | Remove department by ID                            |
| "/rest/employees"                 | GET    | Return all employees                               |
| "/rest/employees/{id}"            | GET    | Return employee by ID                              |
| "/rest/employees"                 | POST   | Create new employee                                |
| "/rest/employees/{id}"           	| PUT    | Update employee by ID                              |
| "/rest/employees/{id}"     	 	    | DELETE | Remove employee by ID                              |
