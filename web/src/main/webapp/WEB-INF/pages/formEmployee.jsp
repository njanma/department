<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/myCss.css"/>" />
<title>Form employee</title>

</head>
<body>
	<div class="center">
		<div class="container col-md-12">

			<div class="panel panel-info">
				<div class="panel-heading">
					<c:choose>
						<c:when test="${employee['new']}">
							<h1>Add employee</h1>
						</c:when>
						<c:otherwise>
							<h1>Update employee</h1>
						</c:otherwise>
					</c:choose>
				</div>
				<br />


				<spring:url value="/employees" var="employeesAction" />
				<form:form class="form-horizontal" method="post"
					modelAttribute="employee" action="${employeesAction}">
					<form:hidden path="id" />

					<div class="form-group">
						<label class="col-sm-4 control-label">Full Name</label>
						<div class="col-sm-4">
							<form:input path="fullName" type="text" class="form-control"
								id="fullName" placeholder="Full Name" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Date Of Birthday</label>
						<div class="col-sm-4">

							<c:choose>
								<c:when test="${employee['new']}">
									<input name="dateOfBirthday" type="date" class="form-control"
										value="yyyy-MM-dd" />
								</c:when>
								<c:otherwise>
									<input name="dateOfBirthday" type="date" class="form-control"
										value="${employee.dateOfBirthday}" />
								</c:otherwise>
							</c:choose>

						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Salary</label>
						<div class="col-sm-4">
							<form:input path="salary" type="number" step="0.01"
								class="form-control" id="salary" placeholder="Salary" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Department</label>
						<div class="col-sm-4">
							<form:select path="departmentId" id="departmentId"
								items="${departments}" cssClass="form-control">
							</form:select>
						</div>
					</div>
					<a href="<spring:url value='/employees' />" 
					class="btn btn-default">Back</a>
							<c:choose>
								<c:when test="${employee['new']}">
									<button type="submit" class="btn btn-primary pull-right">Add
									</button>
								</c:when>
								<c:otherwise>
									<button type="submit" class="btn btn-primary pull-right">Update
									</button>
								</c:otherwise>
							</c:choose>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>