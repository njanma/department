<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<title>Departments</title>

<style type="text/css">
</style>
<link rel="stylesheet" href="<c:url value="/resources/css/myCss.css"/>" />
</head>
<body>
	<div class="center">
		<div class="panel panel-info ">
			<!-- Default panel contents -->
			<div class="panel-heading"><h3>
			<a href="<spring:url value='/departments' />">Departments</a></h3>
			<a href="<spring:url value='/' />"
					class="btn btn-default pull-left">back</a>
			<a href="<spring:url value='/departments/add' />" 
					class="btn btn-default pull-right">add</a>
					</div>

			<!-- Table -->
			<table class="table" style="text-align: center;">
				<thead>
					<tr>
						<th class="col-md-4" style="text-align: center;">department</th>
						<th class="col-md-2" style="text-align: center;">avg salary</th>
						<th class="col-md-6" style="text-align: center;">action</th>
					</tr>
				</thead>
				<c:forEach var="department" items="${departments}">
					<spring:url value="/departments/${department.id}/employees"	var="info" />
					<spring:url value="/departments/${department.id}/delete" var="delete" />
					<spring:url value="/departments/${department.id}/update" var="update" />

					<tr>
						<td>${department.name}</td>
						<td><fmt:formatNumber type="currency" 
						currencySymbol="$" value="${department.salary}" /></td>
						<td>
							<div>
								<form action="${info}" method="GET">
									<button class="btn btn-info col-md-4">Info</button>
								</form>
							</div>
							<div>
								<form action="${update}" method="GET">
									<button class="btn btn-primary col-md-4">Update</button>
								</form>
							</div>
							<div>
								<form action="${delete}" method="POST">
									<button class="btn btn-danger col-md-4">Delete</button>
								</form>
							</div>
						</td>
					</tr>

				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>