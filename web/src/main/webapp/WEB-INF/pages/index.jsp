<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/myCss.css"/>" />
<title>Index</title>

<style type="text/css">
</style>
</head>
<body>
	<div class="center">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>Site endpoints</h2>
			</div>
			<table class="table" >
				<thead>
					<tr>
						<th class="col-md-2">URL</th>
						<th class="col-md-1">Method</th>
						<th class="col-md-5">Description</th>
					</tr>
				</thead>
				
				
				<tr><td>"/web/departments"</td>					<td>GET</td>	<td>Return all departments</td></tr>
				<tr><td>"/web/departments/{id}"</td>			<td>GET</td>	<td>Return department by ID</td></tr>
				<tr><td>"/web/departments/{id}/employees"</td>	<td>GET</td>	<td>Return all employees in department by ID</td></tr>
				<tr><td>"/web/departments/{id}/update"</td>		<td>GET</td>	<td>Return update form for department by ID</td></tr>
				<tr><td>"/web/departments/add"</td>				<td>GET</td>	<td>Return add form for a new department</td></tr>
				<tr><td>"/web/departments/{id}/delete"</td>		<td>POST</td>	<td>Remove department by ID</td></tr>
				<tr><td>"/web/departments"</td>					<td>POST</td>	<td>Create new department</td></tr>
				
				<tr><td>"/web/employees"</td>					<td>GET</td>	<td>Return all employees</td></tr>
				<tr><td>"/web/employees?from="?"&to="?""</td>	<td>GET</td>	<td>Return all employees which birthday between from and to param("y-M-d")</td></tr>
				<tr><td>"/web/employees?from="?"&to="""</td>	<td>GET</td>	<td>Return employee at whom birthday is param from="y-M-d"</td></tr>
				<tr><td>"/web/employees/{id}/update"</td>		<td>GET</td>	<td>Return update form for employee by ID</td></tr>
				<tr><td>"/web/employees/add"</td>				<td>GET</td>	<td>Return add form for a new employee </td></tr>
				<tr><td>"/web/employees/{id}/delete"</td>		<td>POST</td>	<td>Remove employee by ID</td></tr>
				<tr><td>"/web/employees"</td>					<td>POST</td>	<td>Create new employee</td></tr>
			
			</table>
		</div>
	</div>

</body>
</html>