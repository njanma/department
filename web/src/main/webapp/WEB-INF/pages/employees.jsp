<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>Employees</title>
<style type="text/css">
</style>

<link rel="stylesheet" href="<c:url value="/resources/css/myCss.css"/>" />

</head>
<body>
	<div class="center">
		<div class="panel panel-info ">
			<div class="panel-heading">
				<c:choose>
					<c:when test="${fn:startsWith(path,'/web/departments')}">
						<h3>
							<a href="<spring:url value='/employees' />">Employees</a>
						</h3>
						<a href="<spring:url value='/departments' />"
							class="btn btn-default pull-left">back</a>
					</c:when>
					<c:otherwise>
					<h3>
					<a href="<spring:url value='/employees' />">All employees</a>
				</h3>
						<a href="<spring:url value='/' />"
							class="btn btn-default pull-left">back</a>
					</c:otherwise>
				</c:choose>

				<a href="<spring:url value='/employees/add' />"
					class="btn btn-default pull-right">add</a>
			</div>

			<!-- Table -->
			<table class="table" style="text-align: center;">
				<thead>
					<tr>
						<th class="col-md-3" style="text-align: center;">Full Name</th>
						<th class="col-md-3" style="text-align: center;">Birthday(y-M-d)</th>
						<th class="col-md-2" style="text-align: center;">Salary</th>
						<th style="text-align: center;">action</th>
					</tr>
				</thead>
				<c:forEach var="employee" items="${employees}">
					<spring:url value="/employees/${employee.id}/delete" var="delete" />
					<spring:url value="/employees/${employee.id}/update" var="update" />
					<tr>
						<td>${employee.fullName}</td>
						<td>${employee.dateOfBirthday}</td>
						<td><fmt:formatNumber type="currency" currencySymbol="$"
								value="${employee.salary}" /></td>
						<td>
							<div>
								<form action="${update}" method="GET">
									<button class="btn btn-primary col-md-4">Update</button>
								</form>
							</div>
							<div>
								<form action="${delete}" method="POST">
									<button class="btn btn-danger col-md-4">Delete</button>
								</form>
							</div>
						</td>
					</tr>

				</c:forEach>
			</table>
		</div>
		<c:set var="pt" value="${path}" />
		<c:choose>
			<c:when test="${fn:startsWith(path,'/web/departments')}">
			</c:when>
			<c:otherwise>
				<div>
					<c:set value="${path}/employees" var="employees" />
					<form action="" method="get" class="navbar-form navbar-center">
						<!-- 				<div> -->
						<div class="form-group">
							From(or Date of Birthday): <input type="date" name="from">
							To: <input type="date" name="to">
						</div>
						<!-- 					<div> -->
						<button class="btn btn-primary">Search</button>
						<!-- 					</div> -->
						<!-- 				</div> -->
					</form>
				</div>
			</c:otherwise>
		</c:choose>

	</div>


</body>
</html>