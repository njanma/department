<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/myCss.css"/>" />
<title>Form department</title>
</head>
<body>
	<div class="center">
		<div class="container col-md-12">

			<div class="panel panel-info">
				<div class="panel-heading">
					<c:choose>
						<c:when test="${department['new']}">
							<h1>Add department</h1>
						</c:when>
						<c:otherwise>
							<h1>Update department</h1>
						</c:otherwise>
					</c:choose>
				</div>
				<br>

				<spring:url value="/departments" var="departmentsAction" />
				<form:form class="form-horizontal" method="post"
					modelAttribute="department" action="${departmentsAction}">
					<form:hidden path="id" />

					<div class="form-group">
						<label class="col-sm-4 control-label">Name</label>
						<div class="col-sm-4">
							<form:input path="name" type="text" class="form-control"
								id="name" placeholder="Name" />
						</div>
					</div>

					<a href="<spring:url value='/departments' />" 
					class="btn btn-default">Back</a>
					<c:choose>
						<c:when test="${department['new']}">
							<button type="submit" class="btn btn-primary pull-right">Add
							</button>
						</c:when>
						<c:otherwise>
							<button type="submit" class="btn btn-primary pull-right">Update
							</button>
						</c:otherwise>
					</c:choose>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>