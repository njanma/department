
package com.njanma.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.model.Department;
import com.njanma.model.Employee;

/**
 * @author Hryshanau Andrei.
 * 
 * Controller for {@link Employee}.
 */
@Controller
@RequestMapping(value = "/employees")
public class EmployeeController {

	@Value("${client.departments}")
	private String DEPARTMENT_URL;

	@Value("${client.employees}")
	private String EMPLOYEE_URL;

	@Autowired
	private RestTemplate restTemplate;

	Logger log = Logger.getLogger(EmployeeController.class);

	private ObjectMapper mapper;
	{
		mapper = new ObjectMapper();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(LocalDate.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * Get all employees.
	 * 
	 * @param model
	 * @param from begin of interval.
	 * @param to end of interval.
	 * @return view for employees.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAllEmployees(Model model,  HttpServletRequest request,
			@RequestParam(value = "from", required = false) String from,
			@RequestParam(value = "to", required = false) String to)
			throws JsonParseException, JsonMappingException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("getAllEmploees(" + "parameters=" + "from=" + from + ", to=" + to + ")");
		}
		if (from != null && to != null) {
			UriComponents uriComp = UriComponentsBuilder.fromUriString(EMPLOYEE_URL).queryParam("from", from)
					.queryParam("to", to).build();
			String answer = restTemplate.getForObject(uriComp.toUri(), String.class);
			List<Employee> findEmployeesForQuery = Arrays.asList(mapper.readValue(answer, Employee[].class));
			model.addAttribute("employees", findEmployeesForQuery);
		} else {
			String answer = restTemplate.getForObject(EMPLOYEE_URL, String.class);
			List<Employee> allEmployees = Arrays.asList(mapper.readValue(answer, Employee[].class));
			model.addAttribute("employees", allEmployees);
		}
		model.addAttribute("path", request.getRequestURI());
		return "employees";
	}

	/**
	 * Update form employee by ID.
	 * 
	 * @param id employee ID.
	 * @param model
	 * @return view for update employee.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
	public String updateFormEmployeeById(@PathVariable("id") String id, Model model)
			throws JsonParseException, JsonMappingException, IOException {
		if(log.isDebugEnabled()){
			log.debug("updateEmployeeById( id="+id+" )");
		}
		String answer = restTemplate.getForObject(EMPLOYEE_URL + "/" + id, String.class);
		Employee currentEmployee = mapper.readValue(answer, Employee.class);
		model.addAttribute("departments", getAllDepartments());
		model.addAttribute("employee", currentEmployee);
		return "formEmployee";
	}

	/**
	 * Add form for employee.
	 * 
	 * @param model
	 * @return view for add employee.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addFormEmployee(Model model) throws JsonParseException, JsonMappingException, IOException {
		if(log.isDebugEnabled()){
			log.debug("addEmployeeForm()");
		}
		model.addAttribute("employee", new Employee());
		model.addAttribute("departments", getAllDepartments());
		return "formEmployee";
	}

	/**
	 * Delete employee by ID.
	 * 
	 * @param id employee ID.
	 * @return redirect to employees view.
	 */
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
	public String deleteEmployeeById(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteEmployeeById( id=" + id + " )");
		}
		restTemplate.delete(EMPLOYEE_URL + "/{id}", id);
		return "redirect:/employees";
	}

	/**
	 * Create or update employee.
	 * 
	 * @param parameters 
	 * @return redirect to employees view.
	 * @throws JsonProcessingException
	 */
	@RequestMapping(method = RequestMethod.POST, produces=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String createOrUpdateEmployee(@ModelAttribute Employee parameters) throws JsonProcessingException {
		if (log.isDebugEnabled()) {
			log.debug("createOrUpdateEmployee( parameters= " + parameters.toString() + " )");
		}
		String requestBody = mapper.writeValueAsString(parameters);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
		if (parameters.isNew()) {
			restTemplate.postForObject(EMPLOYEE_URL, entity, String.class);
		} else {
			restTemplate.put(EMPLOYEE_URL + "/" + parameters.getId(), entity);
		}
		return "redirect:/employees";
	}

	/**
	 * Get all departments.
	 * 
	 * @return all departments, Map<ID department, name department>.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private Map<Long, String> getAllDepartments() throws JsonParseException, JsonMappingException, IOException {
		String answer = restTemplate.getForObject(DEPARTMENT_URL, String.class);
		List<Department> allDepartments = Arrays.asList(mapper.readValue(answer, Department[].class));
		Map<Long, String> returnMap = new HashMap<>();
		for (Department depart : allDepartments) {
			returnMap.put(depart.getId(), depart.getName());
		}
		return returnMap;
	}

}
