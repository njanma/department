package com.njanma.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.model.Department;
import com.njanma.model.Employee;

/**
 * 
 * @author Hryshanau Andrei.
 * 
 * Controller for {@link Department}.
 *
 */
@Controller
@RequestMapping(value = "/departments")
public class DepartmentController {

	@Value("${client.departments}")
	private String DEPARTMENT_REST;

	@Autowired
	private RestTemplate restTemplate;

	Logger log = Logger.getLogger(DepartmentController.class);

	private ObjectMapper mapper;
	
	{
		mapper = new ObjectMapper();
	}

	/**
	 * Get all departments.
	 * 
	 * @param model
	 * @return view for departments.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAllDepartments(Model model) throws JsonParseException, JsonMappingException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("getAllDepartments()");
		}
		String answer = restTemplate.getForObject(DEPARTMENT_REST, String.class);
		List<Department> departments = Arrays.asList(mapper.readValue(answer, Department[].class));
		model.addAttribute("departments", departments);
		return "departments";
	}

	/**
	 * Get all employees in department by ID.
	 * 
	 * @param id department id.
	 * @param model
	 * @param request
	 * @return view for employees.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/{id}/employees", method = RequestMethod.GET)
	public String getAllEmployeesInDepartmentById(@PathVariable("id") String id, Model model, HttpServletRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("getAllEmployeesInDepartment( id=" + id + " )");
		}
		String answer = restTemplate.getForObject(DEPARTMENT_REST + "/" + id + "/employees", String.class);
		Iterable<Employee> employees = Arrays.asList(mapper.readValue(answer, Employee[].class));
		model.addAttribute("employees", employees);
		model.addAttribute("path", request.getRequestURI());
		return "employees";
	}

	/**
	 * Delete department by ID.
	 * 
	 * @param id department ID.
	 * @return redirect to departments view.
	 */
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
	public String deleteDepartmentById(@PathVariable("id") String id) {
		if (log.isDebugEnabled()) {
			log.debug("deleteDepartmentById( id=" + id + " )");
		}
		restTemplate.delete(DEPARTMENT_REST + "/{id}", id);
		return "redirect:/departments";
	}

	/**
	 * Update form for department by ID.
	 * 
	 * @param id employee ID.
	 * @param model
	 * @return view for update department.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
	public String updateFormDepartmentById(@PathVariable("id") String id, Model model)
			throws JsonParseException, JsonMappingException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("updateEmployeeById( id=" + id + " )");
		}
		String answer = restTemplate.getForObject(DEPARTMENT_REST + "/" + id, String.class);
		Department currentDepartment = mapper.readValue(answer, Department.class);
		model.addAttribute("department", currentDepartment);
		return "formDepartment";
	}

	/**
	 * Add department form.
	 * 
	 * @param model
	 * @return view for add department.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addDepartmentForm(Model model) throws JsonParseException, JsonMappingException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("addEmployeeForm()");
		}
		model.addAttribute("department", new Department());
		return "formDepartment";
	}

	/**
	 * Create or update department.
	 * 
	 * @param parameters department parameters.
	 * @return redirect to departments view.
	 * @throws JsonProcessingException
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String createOrUpdateDepartment(@ModelAttribute(value = "department") Department parameters)
			throws JsonProcessingException, URISyntaxException {
		if (log.isDebugEnabled()) {
			log.debug("createOrUpdateDepartment( parameters= " + parameters.toString() + " )");
		}
		String requestBody = mapper.writeValueAsString(parameters);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
		if (parameters.isNew()) {
			restTemplate.postForObject(DEPARTMENT_REST, entity, String.class);
		} else {
			restTemplate.put(DEPARTMENT_REST + "/" + parameters.getId(), entity);
		}
		return "redirect:/departments";
	}
}
