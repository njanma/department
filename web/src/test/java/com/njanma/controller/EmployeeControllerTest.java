package com.njanma.controller;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withCreatedEntity;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.net.URI;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.dao.DepartmentRepository;
import com.njanma.dao.EmployeeRepository;
import com.njanma.model.Department;
import com.njanma.model.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/contextForTests.xml" })
@Transactional
public class EmployeeControllerTest {

	MockRestServiceServer mockServer;
	MockMvc mockMvc;
	ObjectMapper mapper;
	InternalResourceViewResolver viewResolver;
	
	Employee employeeOne;
	Employee employeeTwo;
	Department departmentOne;
	
	@Value("${client.employees}")
	private String EMPLOYEE_REST;
	
	@Value("${client.departments}")
	private String DEPARTMENT_REST;
	
	@Value("${mvc.employees}")
	private String EMPLOYEE_URL;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	EmployeeController controller;

	@Autowired
	EmployeeRepository repository;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Before
	public void setUp() throws Exception {
		mapper = new ObjectMapper();
		viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setViewResolvers(viewResolver)
				.build();
		
		mockServer = MockRestServiceServer.createServer(restTemplate);
		
		employeeOne = repository.findOne(1l);
		employeeTwo = repository.findOne(2l);
		departmentOne = departmentRepository.findOne(1l);
		
	}


	@Test
	public final void testGetAllEmployees() throws Exception{
		mockServer.expect(requestTo(EMPLOYEE_REST))
			.andExpect(method(HttpMethod.GET))
			.andRespond(
					withSuccess(mapper.writeValueAsString(
							Arrays.asList(employeeOne,employeeTwo)),
							MediaType.APPLICATION_JSON));
	
		mockMvc.perform(get(EMPLOYEE_URL))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employees", 
					hasItem(
							allOf(
									hasProperty("id", is(notNullValue())),
									hasProperty("fullName", is(notNullValue())),
									hasProperty("dateOfBirthday", is(notNullValue())),
									hasProperty("salary", is(notNullValue())),
									hasProperty("departmentId", is(notNullValue()))
							))))
			.andExpect(view().name(is("employees")))
			;
	}
	
	@Test
	public final void testGetAllEmployeesForQueryBetween() throws Exception{
		UriComponents uriComp = UriComponentsBuilder.fromUriString(EMPLOYEE_REST)
				.queryParam("from", "1993-01-01")
				.queryParam("to", "1996-01-01").build();
		mockServer.expect(requestTo(uriComp.toUri()))
			.andExpect(method(HttpMethod.GET))
			.andRespond(
					withSuccess(mapper.writeValueAsString(
							Arrays.asList(employeeTwo)),
							MediaType.APPLICATION_JSON));
	
		mockMvc.perform(get(EMPLOYEE_URL)
				.param("from", "1993-01-01")
				.param("to", "1996-01-01"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employees", 
					hasItem(
							allOf(
									hasProperty("id", is(notNullValue())),
									hasProperty("fullName", is(notNullValue())),
									hasProperty("dateOfBirthday", is(notNullValue())),
									hasProperty("salary", is(notNullValue())),
									hasProperty("departmentId", is(notNullValue()))
							))))
			.andExpect(view().name(is("employees")))
			;
	}
	
	@Test
	public final void testGetAllEmployeesForQueryBirthday() throws Exception{
		UriComponents uriComp = UriComponentsBuilder.fromUriString(EMPLOYEE_REST)
				.queryParam("from", "1991-01-01")
				.queryParam("to", "").build();
		mockServer.expect(requestTo(uriComp.toUri()))
			.andExpect(method(HttpMethod.GET))
			.andRespond(
					withSuccess(mapper.writeValueAsString(
							Arrays.asList(employeeOne)),
							MediaType.APPLICATION_JSON));
		
		mockMvc.perform(get(EMPLOYEE_URL)
				.param("from", "1991-01-01")
				.param("to", ""))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employees", 
					hasItem(
							allOf(
									hasProperty("id", is(notNullValue())),
									hasProperty("fullName", is(notNullValue())),
									hasProperty("dateOfBirthday", is(notNullValue())),
									hasProperty("salary", is(notNullValue())),
									hasProperty("departmentId", is(notNullValue()))
							))))
			.andExpect(view().name(is("employees")))
			;
	}

	@Test
	public final void testUpdateFormEmployeeById() throws Exception{
		mockServer.expect(requestTo(EMPLOYEE_REST+"/1"))
			.andExpect(method(HttpMethod.GET))
			.andRespond(withSuccess(
					mapper.writeValueAsString(employeeOne), 
					MediaType.APPLICATION_JSON)
					);
		mockServer.expect(requestTo(DEPARTMENT_REST))
		.andExpect(method(HttpMethod.GET))
		.andRespond(withSuccess(
					mapper.writeValueAsString(Arrays.asList(departmentOne)), 
					MediaType.APPLICATION_JSON));
		System.out.println(employeeOne.toString());
		mockMvc.perform(get(EMPLOYEE_URL+"/1/update"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employee", allOf(
					hasProperty("id", is(employeeOne.getId())),
					hasProperty("fullName", is(employeeOne.getFullName())),
					hasProperty("dateOfBirthday", is(notNullValue())),
					hasProperty("salary", is(employeeOne.getSalary())),
					hasProperty("departmentId", is(employeeOne.getDepartmentId()))
					)))
			.andExpect(model().attribute("departments", hasEntry(notNullValue(), notNullValue())))
			.andExpect(view().name("formEmployee"))
			;
		
	}

	@Test
	public final void testAddFormEmployee() throws Exception{
		mockServer.expect(requestTo(DEPARTMENT_REST))
		.andExpect(method(HttpMethod.GET))
		.andRespond(withSuccess(
					mapper.writeValueAsString(Arrays.asList(departmentOne)), 
					MediaType.APPLICATION_JSON));
		
		mockMvc.perform(get(EMPLOYEE_URL+"/add"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employee", allOf(
					hasProperty("id", is(nullValue())),
					hasProperty("fullName", is(nullValue())),
					hasProperty("dateOfBirthday", is(nullValue())),
					hasProperty("salary", is(nullValue())),
					hasProperty("departmentId", is(nullValue()))
					)))
			.andExpect(model().attribute("departments", 
					hasEntry(notNullValue(), notNullValue())))
			.andExpect(view().name("formEmployee"))
			;
	}

	@Test
	public final void testDeleteEmployeeById() throws Exception{
		mockServer.expect(requestTo(EMPLOYEE_REST+"/1"))
			.andExpect(method(HttpMethod.DELETE))
			.andRespond(withSuccess());
		
		mockMvc.perform(post(EMPLOYEE_URL+"/1/delete"))
//			.andDo(print())
			.andExpect(status().isFound())
			.andExpect(redirectedUrl(EMPLOYEE_URL))
			;
	
	}

	@Test
	public final void testCreateEmployee() throws Exception{
		URI createdUri = new URI(EMPLOYEE_REST);
		mockServer.expect(requestTo(EMPLOYEE_REST))
			.andExpect(method(HttpMethod.POST))
			.andRespond(withCreatedEntity(createdUri));
		
		mockMvc.perform(post(EMPLOYEE_URL).contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param("id", "")
				.param("fullName", "employeeThree")
				.param("dateOfBirthday", "1990-01-01")
				.param("salary", "500.0")
				.param("departmentId", "2"))
//			.andDo(print())
			.andExpect(status().isFound())
			
			;
	}
	
	@Test
	public final void testUpdateEmployee() throws Exception{
		mockServer.expect(requestTo(EMPLOYEE_REST+"/2"))
			.andExpect(method(HttpMethod.PUT))
			.andRespond(withSuccess());
		
		mockMvc.perform(post(EMPLOYEE_URL).contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param("id", "2")
				.param("fullName", "employeeTwo")
				.param("dateOfBirthday", "1990-01-01")
				.param("salary", "1000.0")
				.param("department_id", "2"))
//			.andDo(print())
			.andExpect(status().isFound())
			
			;
		
	}

}
