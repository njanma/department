package com.njanma.controller;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withCreatedEntity;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.net.URI;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njanma.dao.DepartmentRepository;
import com.njanma.dao.EmployeeRepository;
import com.njanma.model.Department;
import com.njanma.model.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/contextForTests.xml" })
@Transactional
public class DepartmentControllerTest {


	MockRestServiceServer mockServer;
	MockMvc mockMvc;
	
	@Value("${client.departments}")
	private String DEPARTMENT_REST;
	
	@Value("${mvc.departments}")
	private String DEPARTMENT_URL;
	
	@Autowired
	DepartmentController departmentController;

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	Department departmentOne;
	Department departmentTwo;
	
	ObjectMapper mapper;

	@Before
	public void setUp() throws Exception {
		
		InternalResourceViewResolver viewResolver =
                new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        
		mockMvc = MockMvcBuilders.standaloneSetup(departmentController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setViewResolvers(viewResolver)
				.build();
		departmentOne = departmentRepository.findOne(1l);
		departmentTwo = departmentRepository.findOne(2l);
//		employeeOne = employeeRepository.findOne(1l);
		mapper = new ObjectMapper();
		mockServer = MockRestServiceServer.createServer(restTemplate);
	}

	@Test
	public final void testGetAllDepartments() throws Exception {
		 mockServer.expect(requestTo(DEPARTMENT_REST))
		 .andExpect(method(HttpMethod.GET)).andRespond(withSuccess(
				 mapper.writeValueAsString(Arrays.asList(departmentOne, departmentTwo))
				 ,MediaType.APPLICATION_JSON));
		 
		mockMvc.perform(get(DEPARTMENT_URL))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("departments", notNullValue()))
			.andExpect(view().name("departments"))
			;

		mockServer.verify();
	}

	@Test
	public final void testGetAllEmployeesInDepartment() throws Exception {
		Employee employeeOne = employeeRepository.findOne(1l);
		mockServer.expect(requestTo(DEPARTMENT_REST+"/1/employees"))
			.andExpect(method(HttpMethod.GET))
			.andRespond(withSuccess(mapper.writeValueAsString(Arrays.asList(employeeOne)),MediaType.APPLICATION_JSON));
		mockMvc.perform(get(DEPARTMENT_URL+"/1/employees"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("employees", hasItem(hasProperty("id",is(1l)))))
			.andExpect(view().name("employees"))
			;
	}

	@Test
	public final void testDeleteDepartmentById() throws Exception{
		mockServer.expect(requestTo((DEPARTMENT_REST+"/1")))
			.andExpect(method(HttpMethod.DELETE))
			.andRespond(withSuccess());
				
		mockMvc.perform(post(DEPARTMENT_URL+"/1/delete"))
//			.andDo(print())
			.andExpect(status().isFound())
			.andExpect(redirectedUrl(DEPARTMENT_URL));
	}

	@Test
	public final void testUpdateFormDepartmentById() throws Exception{
		mockServer.expect(requestTo(DEPARTMENT_REST+"/1"))
			.andExpect(method(HttpMethod.GET))
			.andRespond(
					withSuccess(mapper.writeValueAsString(departmentOne),
							MediaType.APPLICATION_JSON));
		mockMvc.perform(get(DEPARTMENT_URL+"/1/update"))
//			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(model().attribute("department", hasProperty("id",is(1l))))
			.andExpect(view().name("formDepartment"))
			;
		
	}

	@Test
	public final void testAddDepartmentForm() throws Exception{
		mockMvc.perform(get(DEPARTMENT_URL+"/add"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("department", hasProperty("id",is(nullValue()))));
	}

	@Test
	public final void testCreateDepartment() throws Exception{
		URI createdUri = new URI(DEPARTMENT_REST);
		mockServer.expect(requestTo(DEPARTMENT_REST))
			.andExpect(method(HttpMethod.POST))
			.andRespond(withCreatedEntity(createdUri))
			;
		
		mockMvc.perform(post(DEPARTMENT_URL)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param("id", "")
				.param("name", "newDEPARTMENT"))
//			.andDo(print())
			.andExpect(status().isFound());
	}
	
	@Test
	public final void testUpdateDepartment() throws Exception{
		mockServer.expect(requestTo(DEPARTMENT_REST+"/1"))
			.andExpect(method(HttpMethod.PUT))
			.andRespond(withSuccess());
		
		mockMvc.perform((post(DEPARTMENT_URL)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param("id", "1")
				.param("name", "threeDepartment")))
//			.andDo(print())
			.andExpect(status().isFound());
				
	}

}
