
CREATE SCHEMA IF NOT EXISTS `departments` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `departments` ;

-- -----------------------------------------------------
-- Table `departments`.`Departments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `departments`.`Departments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `departments`.`Employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `departments`.`Employees` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `full_name` VARCHAR(100) NULL,
  `DOB` DATE NULL,
  `salary` DOUBLE NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`, `department_id`),
  INDEX `fk_Employees_Departments_idx` (`department_id` ASC),
  CONSTRAINT `fk_Employees_Departments`
    FOREIGN KEY (`department_id`)
    REFERENCES `departments`.`Departments` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `departments` (`id`, `name`) VALUES 
		('1', 'administration'),
		('2', 'economic');

INSERT INTO `employees` (`id`, `full_name`, `DOB`, `salary`, `department_id`) VALUES 
		('1', 'Cliff Robertson', '1991-01-01', '1000', '1'),
		('2', 'Claire Bloom', '1992-05-15', '1500', '2'),
		('3', 'Edward McNally', '1975-04-02', '950', '1'),
		('4', 'Dick Van Patten', '1988-10-09', '2000', '2'),
		('5', 'Lilia Skala', '1978-08-25', '1350', '1');
