package com.njanma.dao.logger;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerAspect {


		private Logger log = Logger.getLogger(LoggerAspect.class);
		
		@Pointcut("execution(* com.njanma.dao.*.* (..))")
		public void consoleAllPointcut() {
		}

		@Before("consoleAllPointcut()")
		public void debugLevelBefore(JoinPoint joinPoint) {
			if (log.isDebugEnabled()) {
				log.debug(joinPoint.getSignature().getName());
			}
		}
		
		@AfterThrowing("consoleAllPointcut()")
		public void errorLevelAfterThrowing(JoinPoint joinPoint){
			log.error(joinPoint.getSignature().getClass());
		}
}
