package com.njanma.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.njanma.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Long> {
	
	@Query(value="SELECT d.id, d.name, e.salary "
			+ "FROM departments d LEFT JOIN "
			+ "(SELECT department_id,AVG(salary) AS salary FROM employees GROUP BY department_id ) e "
			+ "on d.id = e.department_id", nativeQuery=true)
	List<Department> findAll();
	
	@Query(value="SELECT d.id, d.name, e.salary "
				+ "FROM departments d "
				+ "LEFT JOIN (select department_id,avg(salary) as salary "
					+ "FROM employees "
					+ "GROUP BY department_id) e "
					+ "ON d.id = e.department_id "
					+ "WHERE id = ?1", nativeQuery=true)
	Department findById(Long id);

}
