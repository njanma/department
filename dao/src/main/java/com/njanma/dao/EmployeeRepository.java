package com.njanma.dao;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.data.repository.CrudRepository;

import com.njanma.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

	List<Employee> findByDateOfBirthdayBetween(LocalDate afterDate, LocalDate beforeDate);

	Employee findByDateOfBirthday(LocalDate dateOfBirthday);
	
	Iterable<Employee> findByDepartmentId(Long id);
	
}
