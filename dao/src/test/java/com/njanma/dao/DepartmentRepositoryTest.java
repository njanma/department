package com.njanma.dao;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.njanma.model.Department;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/contextForTest.xml" })
@Transactional
public class DepartmentRepositoryTest {

	@Autowired
	DepartmentRepository departmentRepository;

	@Test
	public final void testSaveEntity() {
		departmentRepository.save(new Department(3l, "departThree"));
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		assertEquals(3, departments.size());
	}

	@Test
	public final void testFindOne() {
		Department department = departmentRepository.findOne(1l);
		assertEquals("departOne", department.getName());
	}

	@Test
	public final void testExists() {
		assertTrue(departmentRepository.exists(1l));
	}

	@Test
	public final void testFindAll() {
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		assertEquals(2, departments.size());
	}

	@Test
	public final void testDeleteID() {
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		assertEquals(2, departments.size());
		departmentRepository.delete(1l);
		departments = (List<Department>) departmentRepository.findAll();
		assertEquals(1, departments.size());
	}
	
	@Test
	public final void testGetAllDepartment(){
		List<Department> departments = departmentRepository.findAll();
		assertThat(departments.size(),is(2));
	}
	
	@Test
	public final void testFindById(){
		Department department = departmentRepository.findById(1l);
		assertThat(department.getSalary(), is(1000.0));
		assertThat(department.getName(), is("departOne"));
	}

}
