package com.njanma.dao;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.njanma.model.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/contextForTest.xml" })
@Transactional
public class EmployeeRepositoryTest {

	@Autowired
	EmployeeRepository repository;

	@Test
	public final void testDeleteById() {
		List<Employee> employees = (List<Employee>) repository.findAll();
		assertThat(employees.size(), is(2));
		repository.delete(2l);
		employees = (List<Employee>) repository.findAll();
		assertThat(employees.size(), is(1));

	}

	@Test
	public final void testExists() {
		assertTrue(repository.exists(1l));
	}

	@Test
	public final void testFindAll() {
		List<Employee> employees = (List<Employee>) repository.findAll();
		assertThat(employees.size(), is(2));
	}

	@Test
	public final void testFindOneById() {
		Employee employee = repository.findOne(1l);
		assertThat(employee.getId(), is(1l));
		assertThat(employee.getFullName(), is("employeeOne"));
		assertThat(employee.getDateOfBirthday(), is(notNullValue()));
		assertThat(employee.getDepartmentId(), is(notNullValue()));
		assertThat(employee.getSalary(), is(notNullValue()));
	}

	@Test
	public final void testSaveEntity() {
		Employee employeeForSave = new Employee();
		employeeForSave.setFullName("employeeThree");
		employeeForSave.setSalary(1500.0);
		employeeForSave.setDateOfBirthday(new LocalDate(1990, 01, 01));
		employeeForSave.setDepartmentId(2l);

		Employee employee = repository.save(employeeForSave);
		List<Employee> employees = (List<Employee>) repository.findAll();
		assertThat(employee.getId(), is(3l));
		assertThat(employees.size(), is(3));
		assertThat(employee.getDateOfBirthday(), is(new LocalDate(1990, 01, 01)));
	}

	@Test
	public final void testFindByDateOfBirthdayBetween() {
		List<Employee> employeesFromDate = repository.findByDateOfBirthdayBetween(
				new LocalDate(1992, 01, 01),
				new LocalDate(1996, 01, 01));
		assertThat(employeesFromDate.size(), is(1));
	}
	
	@Test
	public final void testFindByDateOfBirthday() {
		Employee employeeFromBirthday = repository.findByDateOfBirthday(new LocalDate(1991,01,01));
		assertThat(employeeFromBirthday.getId(), is(1l));
		assertThat(employeeFromBirthday.getFullName(), is("employeeOne"));
	}
	
	@Test
	public final void testFindByDepartmentId(){
		List<Employee> allEmployeeInDepartment = (List<Employee>) repository.findByDepartmentId(1l);
		assertThat(allEmployeeInDepartment.size(), is(1));
	}

}
